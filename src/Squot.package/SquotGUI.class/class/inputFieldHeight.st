layout
inputFieldHeight
	^ [ToolBuilder default inputFieldHeight]
		on: MessageNotUnderstood do: [:e | e return: self buttonBarHeight * 3/5]